# DebConf16

Imported from the debconf-share git-annex repo, and converted to
git-lfs.

# HOWTO

If you just `git clone` the repo, git-lfs will download *everything*
inside it. To do a shallow clone:

1. `sudo apt install git-lfs`
1. `GIT_LFS_SKIP_SMUDGE=1 git clone --config filter.lfs.smudge=true https://salsa.debian.org/debconf-team/public/share/debconf16.git`
1. `cd debconf16`
1. Use it as if it was a normal git repository.
1. To fetch a file, do `git lfs fetch -I <filename>`

# License

There was no explicit license on this content. We assume that the intent
was for it to be redistributeable.
